using OdinSerializer;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using SerializationUtility = OdinSerializer.SerializationUtility;

public class Movement : MonoBehaviour
{
    public PartyMember michi;
    public Save save;
    private Rigidbody2D rb;
    private Animator anim;
    public float x = 0;
    public float y = 0;
    private int velocidad = 5;
    public string lastMov = "S";

    public bool canMove = true;

    private InventoryController inventoryPlayer;

    public void toggleMove() {
        canMove = !canMove;
    }

    private void Awake()
    {
        this.rb = this.GetComponent<Rigidbody2D>();
        this.anim = this.GetComponent<Animator>(); 
    }

    void Start()
    {
          inventoryPlayer = transform.Find("Inventory").GetComponent<InventoryController>();
    }

    void Update()
    {
        input(); 
    } 
      
    void input()
    {
        bool left = false;
        bool right = false;
        bool down = false;
        bool up = false;

        if (canMove) {
            if (Input.GetKeyDown(KeyCode.O))
            {
                SceneManager.LoadScene("CombateMap_1");
            }
            else if (Input.GetKeyDown(KeyCode.P))
            {
                SceneManager.LoadScene("CombateMap_Boss");
            }

            if (Input.GetKey(KeyCode.A))
            {
                this.rb.velocity = new Vector2(-velocidad, this.rb.velocity.y);
                left = true;
            }
            else if (Input.GetKey(KeyCode.D))
            { 
                this.rb.velocity = new Vector2(velocidad, this.rb.velocity.y);
                right = true;
            }
            else
            {
                this.rb.velocity = new Vector2(0, this.rb.velocity.y);
            }

            if (Input.GetKey(KeyCode.S))
            {
                this.rb.velocity = new Vector2(this.rb.velocity.x, -velocidad);
                down = true;
            }
            else if (Input.GetKey(KeyCode.W))
            {
                this.rb.velocity = new Vector2(this.rb.velocity.x, velocidad);
                up = true;
            }
            else
            {
                this.rb.velocity = new Vector2(this.rb.velocity.x, 0);
            }

            if (left == true)
            {
                this.anim.Play("MoveLeft");
                this.lastMov = "A"; 
            }
            else if (right == true)
            {
                this.anim.Play("MoveRight");
                this.lastMov = "D";
            }
            else if (down == true)
            {
                this.anim.Play("MoveDown");
                this.lastMov = "S";
            }
            else if (up == true)
            {
                this.anim.Play("MoveUp");
                this.lastMov = "W"; 
            }
            else 
            { 
                changeIdleAnim();
            } 
             
            if (Input.GetKeyDown(KeyCode.L))
            {
                this.SaveGame();
            } 
            /*
            if (Input.GetKeyDown(KeyCode.I))
            {
                this.michi.AddExp(100); 
            }  

            if (Input.GetKeyDown(KeyCode.K))
            {
                this.michi.ChangeStats(); 
            }*/
             
        }
    }
     
    public void SaveGame()
    {
        this.save.position.scene = SceneManager.GetActiveScene().name;
        this.save.position.position = this.transform.position;
        for (int i = 0;i < inventoryPlayer.items.Count;i++) {
            this.save.inventory.items.Add(inventoryPlayer.items[i]);
        }
        //this.save.inventory.items = inventoryPlayer.items;
        byte[] serializedData = SerializationUtility.SerializeValue<Save>(save, DataFormat.JSON);

        string base64 = System.Convert.ToBase64String(serializedData);
        File.WriteAllText("michifantasy.json", base64);
    }

    public void changeIdleAnim()
    {
        if (this.lastMov == "A")
        {
            this.anim.Play("IdleLeft");
        }
        else if (this.lastMov == "D")
        {
            this.anim.Play("IdleRight");
        } 
        else if (this.lastMov == "S")
        {
            this.anim.Play("IdleDown");
        }
        else if (this.lastMov == "W")
        {
            this.anim.Play("IdleUp");
        }
    }

    public void changePosition(float x, float y)
    {
        this.transform.position = new Vector3(x, y, this.transform.position.z);
    }

}
