using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TabAttack : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
    // Start is called before the first frame update
    public Attackselect Attackselect;

    public void OnPointerClick(PointerEventData eventData)
    {
        Attackselect.OnTabSelected(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // print("TabButton OnPointerEnter");
        Attackselect.OnTabEnter(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Attackselect.OnTabExit(this);
    }

    void Start()
    {
        Attackselect.Subscribe(this);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
