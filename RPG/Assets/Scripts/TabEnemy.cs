using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TabEnemy : MonoBehaviour
{
    // Start is called before the first frame update
    public EnemySelect enemySelect;

    public void OnPointerClick(PointerEventData eventData)
    {
        enemySelect.OnTabSelected(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // print("TabButton OnPointerEnter");
        enemySelect.OnTabEnter(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        enemySelect.OnTabExit(this);
    }

    void Start()
    {
        enemySelect.Subscribe(this);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
