using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    public GameManager_Combate g;
    public TextMeshProUGUI[] nameplayers= new TextMeshProUGUI[4];
    public TextMeshProUGUI[] HPplayers = new TextMeshProUGUI[4];
    public TextMeshProUGUI[] MPplayers = new TextMeshProUGUI[4];
    public Slider[] ready= new Slider[4];





    // Start is called before the first frame update
    void Start()
    {
        g = GameObject.FindObjectOfType<GameManager_Combate>();
          
        nameplayers[0].text = g.Party[0].name;
        HPplayers[0].text = g.Party[0].HP + " / " + g.Party[0].MaxHP;  
        MPplayers[0].text = g.Party[0].MP + " / " + g.Party[0].MaxMP;
        ready[0].GetComponent<Slider>().maxValue = g.PartyCombate[0].GetComponent<PartyCombate>().MaxCarga;
        ready[0].GetComponent<Slider>().value = 0;

        for (int i=0;i< g.Party.Count;i++)
        {
            nameplayers[i].gameObject.SetActive(true);
            nameplayers[i].text = g.Party[i].name;
            ready[i].gameObject.SetActive(true);
            ready[i].GetComponent<Slider>().maxValue = g.PartyCombate[i].GetComponent<PartyCombate>().MaxCarga;
            ready[i].GetComponent<Slider>().value = 0;
            HPplayers[i].gameObject.SetActive(true);
            HPplayers[i].text = Mathf.Round(g.Party[i].HP) + " / " + g.Party[i].MaxHP;
            MPplayers[i].gameObject.SetActive(true);
            MPplayers[i].text = Mathf.Round(g.Party[i].MP) + " / " + g.Party[i].MaxMP;
        }
        //if (g.Party.Count>1)   
        //{
        //    nameplayers[1].gameObject.SetActive(true);
        //    nameplayers[1].text = g.Party[1].name;
        //    ready[1].gameObject.SetActive(true);
        //    ready[1].GetComponent<Slider>().maxValue = g.PartyCombate[1].GetComponent<PartyCombate>().MaxCarga;
        //    ready[1].GetComponent<Slider>().value = 0;
        //    HPplayers[1].gameObject.SetActive(true);
        //    HPplayers[1].text = g.Party[1].HP + " / " + g.Party[1].MaxHP;
        //    MPplayers[1].gameObject.SetActive(true);
        //    MPplayers[1].text = g.Party[1].MP + " / " + g.Party[1].MaxMP;
        //}
        //if (g.Party.Count > 2) 
        //{
        //    nameplayers[1].gameObject.SetActive(true);
        //    nameplayers[2].text = g.Party[2].name;
        //    ready[2].gameObject.SetActive(true);
        //    ready[2].GetComponent<Slider>().maxValue = g.PartyCombate[2].GetComponent<PartyCombate>().MaxCarga;
        //    ready[2].GetComponent<Slider>().value = 0;
        //    HPplayers[2].gameObject.SetActive(true);
        //    HPplayers[2].text = g.Party[2].HP + " / " + g.Party[2].MaxHP;
        //    MPplayers[2].gameObject.SetActive(true);
        //    MPplayers[2].text = g.Party[2].MP + " / " + g.Party[2].MaxMP;  
        //}
        //if (g.Party.Count > 3)
        //{
        //    ready[3].gameObject.SetActive(true);
        //    ready[3].GetComponent<Slider>().maxValue = g.PartyCombate[3].GetComponent<PartyCombate>().MaxCarga;
        //    ready[3].GetComponent<Slider>().value = 0;
        //    HPplayers[3].gameObject.SetActive(true);
        //    nameplayers[3].text = g.Party[3].name;
        //    HPplayers[3].text = g.Party[3].HP + " / " + g.Party[3].MaxHP;
        //    MPplayers[3].gameObject.SetActive(true);
        //    MPplayers[3].text = g.Party[3].MP + " / " + g.Party[3].MaxMP;
        //}
        
    }

    // Update is called once per frame  
    void Update()
    {
        HPplayers[0].text = g.Party[0].HP + " / " + g.Party[0].MaxHP;
        MPplayers[0].text = g.Party[0].MP + " / " + g.Party[0].MaxMP;
        ready[0].GetComponent<Slider>().value = g.PartyCombate[0].GetComponent<PartyCombate>().Carga;
        for (int i=0; i<g.Party.Count;i++) 
        {
            HPplayers[i].text = Mathf.Round(g.Party[i].HP) + " / " + g.Party[i].MaxHP;
            MPplayers[i].text = Mathf.Round(g.Party[i].MP) + " / " + g.Party[i].MaxMP;
            ready[i].GetComponent<Slider>().value = g.PartyCombate[i].GetComponent<PartyCombate>().Carga;
        }
        //if (HPplayers[1].IsActive())
        //{
        //    HPplayers[1].text = g.Party[1].HP + " / " + g.Party[1].MaxHP;
        //    MPplayers[1].text = g.Party[1].MP + " / " + g.Party[1].MaxMP;
        //    ready[1].GetComponent<Slider>().value = g.PartyCombate[1].GetComponent<PartyCombate>().Carga;
        //}  
        //if (HPplayers[2].IsActive())  
        //{
        //    HPplayers[2].text = g.Party[2].HP + " / " + g.Party[2].MaxHP;
        //    MPplayers[2].text = g.Party[2].MP + " / " + g.Party[2].MaxMP;  
        //    ready[2].GetComponent<Slider>().value = g.PartyCombate[2].GetComponent<PartyCombate>().Carga;
        //}
        //if (HPplayers[3].IsActive())
        //{  
        //    HPplayers[3].text = g.Party[3].HP + " / " + g.Party[3].MaxHP;
        //    MPplayers[3].text = g.Party[3].MP + " / " + g.Party[3].MaxMP;
        //    ready[3].GetComponent<Slider>().value = g.PartyCombate[3].GetComponent<PartyCombate>().Carga;
        //}
    }
}
