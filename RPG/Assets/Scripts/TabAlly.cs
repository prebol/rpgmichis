using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TabAlly : MonoBehaviour
{
    
    public AllySelector allySelector;

    public void OnPointerClick(PointerEventData eventData)
    {
        allySelector.OnTabSelected(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // print("TabButton OnPointerEnter");
        allySelector.OnTabEnter(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        allySelector.OnTabExit(this);
    }

    void Start()
    {
        allySelector.Subscribe(this);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
