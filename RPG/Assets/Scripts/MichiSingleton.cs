using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MichiSingleton : MonoBehaviour
{
    static GameObject michiInstance = null;
    void Awake()
    {
        if (michiInstance == null)
        {
            michiInstance = this.gameObject;
            SceneManager.sceneLoaded += onSceneLoaded;
            DontDestroyOnLoad(michiInstance);
        }
        else
        {
            Destroy(this.gameObject);
        }
    } 

    void onSceneLoaded(Scene s, LoadSceneMode lsm) 
    { 
        if (s.name == "CombateMap_Boss" || s.name == "CombateMap_1")
        {
            this.GetComponent<Movement>().x = this.GetComponent<Transform>().transform.position.x;
            this.GetComponent<Movement>().y = this.GetComponent<Transform>().transform.position.y;
            this.GetComponent<Movement>().lastMov = "D";
            this.GetComponent<Movement>().changeIdleAnim();
            this.GetComponent<Transform>().transform.position = new Vector3(-4, 3, 0);
            this.GetComponent<Movement>().enabled = false; 
            this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            this.GetComponent<PartyCombate>().enabled = true;
        } /*
        else if (s.name == "CombateMap_1")
        { 
            this.GetComponent<Movement>().x = this.GetComponent<Transform>().transform.position.x;
            this.GetComponent<Movement>().y = this.GetComponent<Transform>().transform.position.y;
            this.GetComponent<Movement>().lastMov = "D"; 
            this.GetComponent<Movement>().changeIdleAnim();
            this.GetComponent<Transform>().transform.position = new Vector3(-4, 3, 0);
            this.GetComponent<Movement>().enabled = false;
            this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            this.GetComponent<PartyCombate>().enabled = true;
        }*/
        else  
        {
            if (this.GetComponent<Movement>().x != 0 && this.GetComponent<Movement>().y != 0)
            {
                this.GetComponent<Transform>().transform.position = new Vector3(this.GetComponent<Movement>().x, this.GetComponent<Movement>().y,0);
            }
            this.GetComponent<Movement>().lastMov = "S";
            this.GetComponent<Movement>().changeIdleAnim();
            this.GetComponent<Movement>().enabled = true;
            this.GetComponent<PartyCombate>().enabled = false;
        }
    }

}
