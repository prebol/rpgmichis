using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyIA : Bicho
{
    public Enemy enemyinfo;
    public GameManager_Combate GM;
    public float MaxHP;
    public float HP;
    public float Ap;
    public float Atk;
    public float Magic_armor;
    public float Phisyc_armor;
    public float Velociy;
    public float Dodge;
    private float MaxCarga = 100;
    private float Carga = 0;
    private bool enCola = false;

    private float tempAtk;
    private float tempAp;
    private float tempArmor;
    private float tempMr;
    private float tempSpd;
    private float tempDodge;

    public bool alive = true;

    public List<Attacks> attacks;

    //List<PartyCombate> objetives;

    void Start()
    {
        this.GetComponent<SpriteRenderer>().sprite = enemyinfo.sprite;
        this.MaxHP = enemyinfo.MaxHP;
        this.HP = MaxHP; 
        this.Ap = enemyinfo.Ap;
        this.Atk = enemyinfo.Atk;
        this.Magic_armor = enemyinfo.Magic_armor;
        this.Phisyc_armor = enemyinfo.Phisyc_armor;
        this.Velociy = enemyinfo.Velociy;
        this.Dodge = enemyinfo.Dodge;
        this.attacks = enemyinfo.attacks; 

        if (enemyinfo.boss)
        {
            this.GetComponent<Animator>().Play("Idle");
        }
        
    }

    private void Update()
    {
        if (this.Carga < this.MaxCarga)
        {
            this.Carga += 0.03f;
            //print(this.name+": "+Carga);
        }
        else if (this.Carga > this.MaxCarga)
        {
            this.Carga = this.MaxCarga;
        }
        else if (this.Carga == this.MaxCarga)
        {
            if (!enCola)
            {
                //print(this.name + " CARGADO");
                AhoraMeTocaAMi();
            }
        }
    }

    public void GetAttack(Attacks attack, float dmg, string name)
    {
        int x = Random.Range(0, 100);
        if (x < (this.Dodge + this.tempDodge))
        {
            print("DODGED"); 
            GM.NotifyAttack(attack, name, this.name, 0, this.transform.position.x, this.transform.position.y, true);
        }
        else
        {
            print("ANTES: "+this.HP+ " DMG RECIBIDO: "+dmg);
            if (attack.type == AttackType.Magical)
            {
                this.HP -= dmg * (100 / (this.Magic_armor + this.tempMr + 100));
                GM.NotifyAttack(attack, name, this.name, dmg * (100 / (this.Magic_armor + this.tempMr + 100)), this.transform.position.x, this.transform.position.y,false);
            }
            else if (attack.type == AttackType.Physical)
            {
                this.HP -= dmg * (100 / (this.Phisyc_armor + this.tempArmor + 100));
                GM.NotifyAttack(attack, name, this.name, dmg * (100 / (this.Phisyc_armor + this.tempArmor + 100)), this.transform.position.x, this.transform.position.y,false);
            }
            else if (attack.type == AttackType.Else)
            {
                this.HP -= dmg;
                GM.NotifyAttack(attack, name, this.name, dmg, this.transform.position.x, this.transform.position.y,false);
            }
            print("DESPUES: "+this.HP);
            this.tempAtk += attack.addAtk;
            this.tempAp += attack.addAp;
            this.tempArmor += attack.addArmor;
            this.tempMr += attack.addMr; 
            this.tempSpd += attack.addSpd;
            this.tempDodge += attack.addDodge;
        }
        print(this.HP);
        if (this.HP < 0)
        {
            this.HP = 0;
            Defeat();
        }
    }

    public IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(3f);
        this.DoAttack();
    }

    public void DoAttack()
    {
        print("TURNO DE ATAQUE ENEMIGO DE "+this.name);
        int[] prob=new int[4];

        prob[0] = 0;
        prob[1] = 0;
        prob[2] = 0;
        prob[3] = 0;

        int x = Random.Range(0, 100);

        int partymemberObjetive = Random.Range(0,GM.PartyCombate.Length);

        for (int i = 0; i < attacks.Count; i++) 
        {
            prob[i] = attacks[i].probability;
        }
        for (int i = 0; i < GM.PartyCombate.Length; i++)
        {
            print(GM.PartyCombate[i]);
        } 

        if (x < prob[3])
        {
            Formula(GM.PartyCombate[partymemberObjetive],3);
        }
        else if (x < prob[2])
        {
            Formula(GM.PartyCombate[partymemberObjetive], 2);
        }
        else if (x < prob[1])
        {
            Formula(GM.PartyCombate[partymemberObjetive], 1);
        }
        else 
        {
            Formula(GM.PartyCombate[partymemberObjetive], 0);
        }
    }  

    private void Formula(GameObject partyCombate, int v) 
    {
        bool correcto = false;
        GameObject objective = partyCombate;
        while (!correcto)
        { 
            if (objective.gameObject.activeSelf)
            {
                print("CALCULANDO DA�O ENEMIGO: " + this.name + " TO " + partyCombate);
                if (attacks[v].type == AttackType.Magical)
                {
                    objective.GetComponent<PartyCombate>().GetAttack(attacks[v], attacks[v].damage * ((this.Ap + this.tempAp + 100) / 100), this.name);
                }
                else if (attacks[v].type == AttackType.Physical)
                {
                    objective.GetComponent<PartyCombate>().GetAttack(attacks[v], attacks[v].damage * ((this.Atk + this.tempAtk + 100) / 100), this.name);
                }
                else if (attacks[v].type == AttackType.Else)
                {
                    objective.GetComponent<PartyCombate>().GetAttack(attacks[v], attacks[v].damage, this.name);
                }
                correcto = true;
            }
            else
            {
                int index = Random.Range(0, GM.PartyCombate.Length);
                objective = GM.PartyCombate[index];
            }
        }
        this.EndTurn();
    }

    private void EndTurn()
    {
        this.Carga = 0;
        this.enCola = false;
        this.GM.endTurn();
    }

    private void AhoraMeTocaAMi()
    {
        enCola = true;
        GM.chargedTurn(this);
    }

    private void Defeat() 
    {
        GM.expTotal = +enemyinfo.exp;
        GM.DeleteObjective(this);
    }

}
