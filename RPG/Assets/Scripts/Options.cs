using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using OdinSerializer;

public class Options : MonoBehaviour
{
    public void NewGame()
    {
        ChangeSceneManager.Instance.cargarScene("Mapa_Nieve");
    }

    public void ChargeGame()
    {
        string newBase64 = File.ReadAllText("michifantasy.json");
        if (newBase64 == null) {
            print("No hay partida guardada...");
        } else {
            print("Cargando partida");
            byte[] postFile = System.Convert.FromBase64String(newBase64);
            Save newData = SerializationUtility.DeserializeValue<Save>(postFile, DataFormat.JSON);
            ChangeSceneManager.Instance.cargarScene(newData.position.scene);
            this.transform.position = newData.position.position;
        }
        
    }

    public void Exit() 
    {
        Application.Quit();
    }

}
 