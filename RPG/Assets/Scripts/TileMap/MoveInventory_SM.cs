using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveInventory_SM : MonoBehaviour
{
    public CanvasRenderer[] allItems;
    private CanvasRenderer grid;
    private MoveInventory inventoryContainer;
    private GameObject michi;

    public bool subMenuOpen = false;
    [SerializeField] private PartyMember michiPM;

    public int index = 0;
    public int itemIndex = 0;
    void Awake()
    {
        michi = GameObject.Find("Michi");

        grid = transform.Find("grid").GetComponent<CanvasRenderer>();

        inventoryContainer = transform.GetComponentInParent<MoveInventory>();

    }

    // Update is called once per frame
    void Update()
    {
        if (subMenuOpen) {
            moveThrowItems();

            if (Input.GetKeyDown(KeyCode.Return)) {
                switch(index) {
                    case 0:
                        option0();
                        break;
                    case 1:
                        option1();
                        break;
                    case 2:
                        option2();
                        break;
                }
            }
            if (Input.GetKeyDown(KeyCode.Backspace)) {
                option2();
            }
        }
    }

    public void moveThrowItems() {

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {      //ARRIBA
            index--;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {      //ABAJO
            index++;
        }

        int len = allItems.Length;
        if (index < 0) index = 0;
        if (index >= len) index = len-1;
        for (int i = 0;i < len;i++) {
            if (i == index) {
                allItems[i].GetComponent<Image>().color = new Color(0.2924528f, 0.1878137f, 0);  //HACE HIGHLIGHT EN EL ITEM EN EL QUE ESTAS
            } else {
                allItems[i].GetComponent<Image>().color = new Color(0.4433962f, 0.2902472f, 0); //EL RESTO LOS PONE EN COLOR DEFAULT
            }
        }
    }

    public void option0() {
        print("EQUIPAR ARMOR");
        if (michi.transform.Find("Inventory").GetComponent<InventoryController>().items[itemIndex] is RPGArmor) {
            print("Item antiguo: " + michiPM.Equipment[1].name);
            michiPM.ChangeEquipment(michi.transform.Find("Inventory").GetComponent<InventoryController>().items[itemIndex]);
            print("Item nuevo: " + michi.transform.Find("Inventory").GetComponent<InventoryController>().items[itemIndex].name);
            /*
            if (oldItem != null) {
                michi.transform.Find("Inventory").GetComponent<InventoryController>().removeItem(itemAEquipar);
                michi.transform.Find("Inventory").GetComponent<InventoryController>().addItem(oldItem);
            } else {
                print("NO HABIA NINGUN ITEM EQUIPADO");
            }*/
        } else {
            print("Este item no es una armadura, no se puede equipar.");
        }
    }

    public void option1() {
        print("Item Info: " + michi.transform.Find("Inventory").GetComponent<InventoryController>().items?[itemIndex]);
    }

    public void option2() {
        this.subMenuOpen = false;
        inventoryContainer.inventoryOpen = true;
        this.gameObject.SetActive(false);
    }
}
