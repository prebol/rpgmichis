using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveInventory : MonoBehaviour
{
    private Transform[] allItems;

    private CanvasRenderer[] allChildren;
    private CanvasRenderer grid, selector, subMenu;
    private GameObject michi;

    public bool inventoryOpen = false;

    public int index = 0;

    private int itemsCount = 0;
    void Awake()
    {
        michi = GameObject.Find("Michi");

        grid = transform.Find("grid").GetComponent<CanvasRenderer>();
        selector = transform.Find("selector").GetComponent<CanvasRenderer>();

        allChildren = grid.transform.GetComponentsInChildren<CanvasRenderer>();

        allItems = new Transform[grid.transform.childCount];

        subMenu = transform.Find("subMenu").GetComponent<CanvasRenderer>();

        int i = 0;

        foreach (Transform t in grid.transform) {
            allItems[i] = t;
            // print("item: " + allItems[i].transform.position.x);
            i++;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (inventoryOpen) {
            moveThrowItems();

            if (Input.GetKeyDown(KeyCode.Return)) {
                if ((index >= 0 || index <= michi.transform.Find("Inventory").GetComponent<InventoryController>().items.Count-1) && (michi.transform.Find("Inventory").GetComponent<InventoryController>().items?[index] != null)) {
                    this.inventoryOpen = false;
                    subMenu.GetComponent<MoveInventory_SM>().subMenuOpen = true;
                    subMenu.GetComponent<MoveInventory_SM>().itemIndex = index;
                    subMenu.GetComponent<MoveInventory_SM>().index = 0;
                    subMenu.gameObject.SetActive(true);
                    print("item seleccionado: " + michi.transform.Find("Inventory").GetComponent<InventoryController>().items?[index]);
                } else {
                    print("En este slot no hay ningun item.");
                }
                
            }
        }
    }

    public void moveThrowItems() {

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {  //DERECHA
            index++;
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {   //IZQUIERDA
            index--;
        }
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {      //ARRIBA
            index-=8;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {      //ABAJO
            index+=8;
        }

        int len = allItems.Length;
        if (index < 0) index = 0;
        if (index >= len) index = len-1;
        selector.transform.position = allItems[index].transform.position;
    }
}
