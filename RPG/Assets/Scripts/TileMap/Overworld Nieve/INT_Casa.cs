using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class INT_Casa : Interaccion
{
    public AudioClip tiendaMusic;
    public override bool interactuar() {
        //print($"{this.GetComponent<Interaccion>().transform.name}: Has entrado a la casa");
        StartCoroutine(cambiarScene());
        ChangeSceneManager.Instance.activarTransicion();
        return true;
    }

    IEnumerator cambiarScene() {
        yield return new WaitForSeconds(1.5f);
        ChangeSceneManager.Instance.cargarScene("Tienda_Inside");
        GameObject.Find("Michi").GetComponent<Movement>().changePosition(0.39f, -2.7f);
    }
}
