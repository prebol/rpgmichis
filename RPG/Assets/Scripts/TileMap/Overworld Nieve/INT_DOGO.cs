using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class INT_DOGO : Interaccion
{
    public PartyMember perro;
    GameManager gM;
    public string textToDisplay;
    public int tama�oTxt;
    public bool progresivo;
    public float velocidadTxt;
    private CanvasRenderer msgCanvas;
    private TextMeshProUGUI msgTxt;
    private GameObject michi;

    private void Start()
    {
        
        michi = GameObject.Find("Michi");
        msgCanvas = transform.Find("Canvas").transform.Find("msg").GetComponent<CanvasRenderer>();
        msgTxt = msgCanvas.transform.Find("msgTxt").GetComponent<TextMeshProUGUI>();

        msgTxt.fontSize = tama�oTxt;

        gM = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (gM.Story.perroJoined) 
        {
            Destroy(this.gameObject);  
        }  
    }

    public override bool interactuar()
    {
        print("sazfdsafdsdf");
        if (!gM.Story.quest0)    
        {  
            cartelOpen = true; 
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Hola, bienvenido al pueblo, debes aprender a luchar para sobrevivir, deja que te ense�e";
            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else
            {
                mostrarCartelInsta();
            }

            return true;  
        }
        else 
        {
            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Ahora que sabes como luchar me unire a ti para ayudarte en tu aventura";

            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else
            {
                mostrarCartelInsta();
            }

            return true;

        }


    }
    
    public bool cartelOpen = false;

   

    void Update()
    {
        if (cartelOpen)
        {
            if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape))
            {
                ocultarCartel();
            }
        }

    }
      
    

    IEnumerator mostrarCartelProgresivo()
    {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = "";
        foreach (char c in textToDisplay.ToCharArray())
        {
            msgTxt.text += c;
            yield return new WaitForSeconds(velocidadTxt);
        }
    }

    public void mostrarCartelInsta()
    {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = textToDisplay;
    }

    public void ocultarCartel()
    {
        StopAllCoroutines();
        michi.GetComponent<Movement>().canMove = true;
        msgCanvas.gameObject.SetActive(false);
        cartelOpen = false;
        if (!gM.Story.quest0) 
        {
            gM.Story.quest0 = true;
            SceneManager.LoadScene("CombateMap_1");
        }
        else
        {
            gM.Story.perroJoined = true;
            gM.ActiveParty.Perro = perro;  
            Destroy(this.gameObject);  
        }  
        
    }
}
