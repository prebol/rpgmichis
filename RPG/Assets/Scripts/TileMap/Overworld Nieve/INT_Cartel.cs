using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class INT_Cartel : Interaccion
{
    public string textToDisplay;
    public int tamanoTxt;
    public bool progresivo;
    public float velocidadTxt;
    private CanvasRenderer msgCanvas;
    private TextMeshProUGUI msgTxt;
    private GameObject michi;

    public bool cartelOpen = false;

    void Start() {
        michi = GameObject.Find("Michi");
        msgCanvas = transform.Find("Canvas").transform.Find("msg").GetComponent<CanvasRenderer>();
        msgTxt = msgCanvas.transform.Find("msgTxt").GetComponent<TextMeshProUGUI>();

        msgTxt.fontSize = tamanoTxt;
    }

    void Update() {
        if (cartelOpen) {
            if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape)) {
                ocultarCartel();
            }
        }
        
    }
    public override bool interactuar()
    {
        cartelOpen = true;
        michi.GetComponent<Movement>().canMove = false;
        if (progresivo) {
            StartCoroutine(mostrarCartelProgresivo());
        } else {
            mostrarCartelInsta();
        }
        
        return true;
    }

    IEnumerator mostrarCartelProgresivo() {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = "";
        foreach (char c in textToDisplay.ToCharArray()) {
            msgTxt.text += c;
            yield return new WaitForSeconds(velocidadTxt);
        }
    }

    public void mostrarCartelInsta() {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = textToDisplay;
    }

    public void ocultarCartel() {
        StopAllCoroutines();
        michi.GetComponent<Movement>().canMove = true;
        msgCanvas.gameObject.SetActive(false);
        cartelOpen = false; 
    }

    

}
