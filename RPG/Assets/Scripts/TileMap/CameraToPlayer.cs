using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraToPlayer : MonoBehaviour
{
    public static CameraToPlayer Instance;

    void Awake()
    {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }
    
    void Start()
    {
        transform.SetParent(GameObject.Find("Michi").transform);
        float x = GameObject.Find("Michi").transform.position.x;
        float y = GameObject.Find("Michi").transform.position.y;
        transform.position = new Vector3(x, y, -10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
