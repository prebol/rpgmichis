using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class INT_SalirTienda : Interaccion
{
    public AudioClip musicaOverworld;

    public override bool interactuar() {
        StartCoroutine(cambiarScene());
        SoundManager.Instance.PlayMusic(musicaOverworld);
        ChangeSceneManager.Instance.activarTransicion();
        return true;
    }

    IEnumerator cambiarScene() {
        yield return new WaitForSeconds(1.5f);
        ChangeSceneManager.Instance.cargarScene("Mapa_Nieve");
        GameObject.Find("Michi").GetComponent<Movement>().changePosition(-14.49f, 3.32f);
    }
}
