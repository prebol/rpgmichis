using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class INT_Tienda : Interaccion
{
    [SerializeField] private CanvasRenderer tiendaUI;
    private GameObject michi;

    void Start() {
        michi = GameObject.Find("Michi");
    }
    public override bool interactuar() {
        print($"{this.GetComponent<Interaccion>().transform.name}: Has entrado a la tienda");
        tiendaUI.gameObject.SetActive(true);
        michi.GetComponent<Movement>().toggleMove();
        return true;
    }
}
