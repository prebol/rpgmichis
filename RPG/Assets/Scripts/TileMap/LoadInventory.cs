using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LoadInventory : MonoBehaviour
{
    public InventoryController inventory;

    private CanvasRenderer[] allChildren;

    private CanvasRenderer grid;

    [SerializeField] private Sprite slotSprite;

    void Awake() {

        grid = transform.Find("grid").GetComponent<CanvasRenderer>();
        // print("GRID NAME: " + grid.transform.name);
        allChildren = grid.transform.GetComponentsInChildren<CanvasRenderer>();

        for (int i = 1;i < allChildren.Length;i+=2) {   //EL INVENTARIO TIENE 32 SLOTS
            allChildren[i].transform.GetComponent<Image>().sprite = slotSprite; //MODIFICA EL SPRITE DEL SLOT
            allChildren[i].transform.GetChild(0).GetComponent<Image>().color = new Color(255, 255, 255, 0); //le quita la transparencia para que no se vea el cuadrado blanco en los slots vacios
            allChildren[i].transform.GetChild(0).GetComponent<Image>().sprite = null;   //AL EMPEZAR SE LE BORRAN TODOS LOS ITEMS
                                                                                            //TODO - Que en vez de borrarse se llene con los datos guardados
        }
        //loadInventory();
    }

    // void OnEnable() {
    //     loadInventory();
    // }
    
    public void loadInventory() {
        for (int j = 1;j < allChildren.Length;j+=2) {
            allChildren[j].transform.GetChild(0).GetComponent<Image>().sprite = null;
            allChildren[j].transform.GetChild(0).GetComponent<Image>().color = new Color(255, 255, 255, 0);
        }
        print("CARGANDO EL INVENTARIO...");
        int itemLista = 0;
        int i = 1;
        while(itemLista < inventory.items.Count) {
                print($"item lista: {itemLista}");
                allChildren[i].transform.GetChild(0).GetComponent<Image>().sprite = inventory?.items[itemLista].sprite;
                allChildren[i].transform.GetChild(0).GetComponent<Image>().color = new Color(255, 255, 255, 255);
                itemLista++;
                i+=2;
                //print(inventory.items[itemLista].name);
        }
        
    }

    public void setInventoryToLoad(InventoryController inventory) {
        this.inventory = inventory;
    }
}
