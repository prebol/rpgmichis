using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlInventory : MonoBehaviour
{
    [SerializeField] private InventoryController inventory;
    [SerializeField] private CanvasRenderer inventoryGUI, escMenu;
    private GameObject michi;
    // Start is called before the first frame update
    void Start()
    {
        michi = GameObject.Find("Michi");
        inventory = transform.Find("Inventory").GetComponent<InventoryController>();
        inventoryGUI = transform.Find("Canvas").transform.Find("all").transform.Find("inventoryContainer").GetComponent<CanvasRenderer>();
        escMenu = transform.Find("Canvas").transform.Find("all").transform.Find("ingameMenu").GetComponent<CanvasRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            if (!inventoryGUI.gameObject.activeSelf) {
                inventoryGUI.gameObject.SetActive(true);
                michi.GetComponent<Movement>().canMove = false;
                michi.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                inventoryGUI.GetComponent<LoadInventory>().inventory = inventory;
                inventoryGUI.GetComponent<LoadInventory>().loadInventory();
                inventoryGUI.GetComponent<MoveInventory>().inventoryOpen = true;
                inventoryGUI.GetComponent<MoveInventory>().index = 0;
            } else {
                inventoryGUI.gameObject.SetActive(false);
                michi.GetComponent<Movement>().canMove = true;
                inventoryGUI.GetComponent<MoveInventory>().inventoryOpen = false;
            }
            
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            escMenu.gameObject.SetActive(true);
            michi.GetComponent<Movement>().canMove = false;
            escMenu.GetComponent<MoveInGameMenu>().menuOpen = true;
            escMenu.GetComponent<MoveInGameMenu>().index = 0;
        }
    }
}
