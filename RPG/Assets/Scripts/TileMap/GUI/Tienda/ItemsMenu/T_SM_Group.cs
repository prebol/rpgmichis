using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class T_SM_Group: MonoBehaviour
{
    [SerializeField] private CanvasRenderer selector, popUp, shopWindow, mainMenu;    //SPRITE SELECCIONAR CASILLA + POPUP SUBMENU
    private List<T_SM_Button> tabButtons;

    public int index = 0;  //NUM ELEMENTO EN EL QUE ESTA EL JUGADOR
    public bool selecting = false; //TRUE - HAY UN ELEMENTO SELECCIONADO / FALSE - NO HAY NINGUN ELEMENTO SELECCIONADO

    public void Start()
    {

    }
    public void Subscribe(T_SM_Button button)
    {
        if (tabButtons == null)
        {
            tabButtons = new List<T_SM_Button>();
        }

        tabButtons.Add(button);
    }

    public void OnTabEnter(T_SM_Button button)
    {
        
 
    }

    public void OnTabExit(T_SM_Button button)
    {
        shopWindow.gameObject.SetActive(false);
        mainMenu.GetComponent<T_MM_Group>().toggleSelected();
    }

    public void OnTabSelected(T_SM_Button button)
    {
        popUp.gameObject.SetActive(true);
        selecting = true;
        print("Item seleccionado");
    }

    void Update() {

        if (!selecting) {
            controlSeleccion();

            //SELECCIONAR ITEM/ENTRAR OPCIONES ITEM
            if (Input.GetKeyDown(KeyCode.Return)) {
                this.OnTabSelected(tabButtons[index]);
            }
            if (Input.GetKeyDown(KeyCode.Backspace)) {
                this.OnTabExit(tabButtons[index]);
            }
        }
        
    }

    private void controlSeleccion() {

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {  //DERECHA
            index++;
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {   //IZQUIERDA
            index--;
        }
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {      //ARRIBA
            index-=6;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {      //ABAJO
            index+=6;
        }

        int len = tabButtons.Count;
        if (index < 0) index = 0;
        if (index >= len) index = len-1;
        selector.transform.position = transform.GetChild(index).transform.position;
        if (((index+1) % 6) == 0) {
            popUp.transform.position = new Vector2(transform.GetChild(index).transform.position.x-1.5f, transform.GetChild(index).transform.position.y-1);
        } else {
            popUp.transform.position = new Vector2(transform.GetChild(index).transform.position.x+1.5f, transform.GetChild(index).transform.position.y-1);
        }

    }

}
