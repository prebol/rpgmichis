using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenusManager : MonoBehaviour
{
    [SerializeField] private GameObject michi;
    [SerializeField] private CanvasRenderer allTienda, T_MM_Group,
            shopWindow, itemsContainer, T_SM_Group, T_SM_selector, T_SM_confirm, T_SM_2_Group;
    [SerializeField] private InventoryController michiInventory, tiendaInventory;
    [SerializeField] private TiendaManager tiendaManager;
    [SerializeField] private TextMeshProUGUI comprarTxt, venderTxt, titleTxt, sbmTxt;
    
    void Start() {
        michi = GameObject.Find("Michi");
        michiInventory = GameObject.Find("Michi").transform.Find("Inventory").GetComponent<InventoryController>();
    }
    public void mostrarMainMenu() {
        allTienda.gameObject.SetActive(true);
        T_MM_Group.GetComponent<T_MM_Group>().index = 0;
        michi.GetComponent<Movement>().canMove = false;
    }

    public void ocultarMainMenu() {
        allTienda.gameObject.SetActive(false);
        michi.GetComponent<Movement>().canMove = true;
    }

    public void mostrarShopWindow(string tipo) {
        //tiendaManager.cargarDefaultTienda();
        shopWindow.gameObject.SetActive(true);
        switch(tipo) {
            case "comprar":
                itemsContainer.GetComponent<LoadInventory>().setInventoryToLoad(tiendaInventory);
                comprarTxt.gameObject.SetActive(true);
                venderTxt.gameObject.SetActive(false);
                titleTxt.text = "COMPRAR";
                sbmTxt.text = "Comprar";
                T_SM_2_Group.GetComponent<T_SM_2_Group>().comprar = true;
                // T_SM_2_Group.GetComponent<T_SM_2_Group>().transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Comprar";
                break;
            case "vender":
                itemsContainer.GetComponent<LoadInventory>().setInventoryToLoad(michiInventory);
                comprarTxt.gameObject.SetActive(false);
                venderTxt.gameObject.SetActive(true);
                titleTxt.text = "VENDER";
                sbmTxt.text = "Vender";
                T_SM_2_Group.GetComponent<T_SM_2_Group>().comprar = false;
                // T_SM_2_Group.GetComponent<T_SM_2_Group>().transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Vender";
                break;
        }
        itemsContainer.GetComponent<LoadInventory>().loadInventory();
        T_SM_Group.GetComponent<T_SM_Group>().index = 0;
    }

    public void actualizarInventarioTienda() {
        itemsContainer.GetComponent<LoadInventory>().loadInventory();
    }

    public void ocultarShopWindow() {
        shopWindow.gameObject.SetActive(false);
    }

    public void mostrarT_SM_confirm () {
        T_SM_confirm.gameObject.SetActive(true);
        T_SM_Group.GetComponent<T_SM_Group>().selecting = true;
    }

    public void ocultarT_SM_confirm () {
        T_SM_confirm.gameObject.SetActive(false);
        T_SM_Group.GetComponent<T_SM_Group>().selecting = false;
    }
}
