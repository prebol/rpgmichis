using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiendaManager : MonoBehaviour
{
    private InventoryController inventory;

    private AllItemsManager allItems;

    void Awake()
    {
        inventory = transform.Find("Inventory").GetComponent<InventoryController>();
        allItems = AllItemsManager.Instance;
        setItemsTienda();
    }

    public void setItemsTienda() {      //ITEMS QUE TIENE LA TIENDA, PONED LO QUE QUERAIS
        inventory.addItem(allItems.revivir);
        inventory.addItem(allItems.hpPotion);
        inventory.addItem(allItems.varaFuego);
        inventory.addItem(allItems.baculoMadera);
        inventory.addItem(allItems.arcoFuego);
        inventory.addItem(allItems.arcoMadera);
        inventory.addItem(allItems.mallasFurtivas);
        inventory.addItem(allItems.mallasOxidadas);
        inventory.addItem(allItems.mallasNormales);
        inventory.addItem(allItems.antifazDivino);
        inventory.addItem(allItems.capuchaOscura);
        inventory.addItem(allItems.espadaMaderaEscudo);
        inventory.addItem(allItems.dagaMadera);
        inventory.addItem(allItems.dagaClasica);
        inventory.addItem(allItems.diademaLigera);
        inventory.addItem(allItems.diademaRota);
        inventory.addItem(allItems.plumaFurtiva);
        inventory.addItem(allItems.plumaDesgastada);
        inventory.addItem(allItems.plumaCazador);
        inventory.addItem(allItems.vitaminaC);
    }
}
