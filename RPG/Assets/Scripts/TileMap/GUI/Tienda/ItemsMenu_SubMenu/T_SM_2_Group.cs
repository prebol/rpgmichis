using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class T_SM_2_Group: MonoBehaviour
{
    private List<T_SM_2_Button> tabButtons;

    private int index = 0;  //NUM ELEMENTO EN EL QUE ESTA EL JUGADOR EN ESTE MENU

    [SerializeField] private InventoryController playerInventory, tiendaInventory;  //INVENTARIO DE LA TIENDA CON TODOS LOS ITEMS
    [SerializeField] private T_SM_Group tienda;

    [SerializeField] private MenusManager menusManager;
    public int itemToBuy;  //EL INDICE DENTRO DEL INVENTARIO DEL ITEM QUE HA SELECCIONADO EL JUGADOR

    public bool comprar = true;

    void Start()
    {
        playerInventory = GameObject.Find("Michi").transform.Find("Inventory").GetComponent<InventoryController>();
    }
    public void Subscribe(T_SM_2_Button button)
    {
        if (tabButtons == null)
        {
            tabButtons = new List<T_SM_2_Button>();
        }

        tabButtons.Add(button);
    }

    public void OnTabEnter(T_SM_2_Button button)
    {
        
 
    }

    public void OnTabExit(T_SM_2_Button button)
    {
        
    }

    public void OnTabSelected(T_SM_2_Button button)
    {
        
    }

    void OnEnable() {
        index = 0;  //REINICIA PARA QUE SIEMPRE ESTE SELECCIONADA LA PRIMERA OPCION AL PRINCIPIO
    }

    void Update() {

        moveThrowItems();

        //SELECCIONAR OPCION
        if (Input.GetKeyDown(KeyCode.Return)) {
            optionSelected();
        }
        //DESELECCIONAR ITEM/ VOLVER AL MENU PRINCIPAL DE ITEMS
        if (Input.GetKeyDown(KeyCode.Backspace)) {
            optionCerrar();
        }

    }

    //PARA CAMBIAR ENTRE LAS OPCIONES DEL MENU + 'HOVER'
    public void moveThrowItems() {

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
            index--;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {
            index++;
        }

        int len = tabButtons.Count;
        if (index < 0) index = 0;
        if (index >= len) index = len-1;
        for (int i = 0;i < len;i++) {
            if (i == index) {
                tabButtons[i].GetComponent<Image>().color = new Color(0.2924528f, 0.1878137f, 0);  //HACE HIGHLIGHT EN EL ITEM EN EL QUE ESTAS
            } else {
                tabButtons[i].GetComponent<Image>().color = new Color(0.4433962f, 0.2902472f, 0); //EL RESTO LOS PONE EN COLOR DEFAULT
            }
            //print($"i: {i}, index: {index}");
        }
    }

    //GESTIONA LA ACCION QUE HACE CUANDO LE DAS A UNO DE LOS BOTONES
    public void optionSelected() {
        switch(index) {
            case 0: //COMPRAR - Compra el item y vuelve
                if (comprar) {
                    optionComprar();
                } else {
                    optionVender();
                }
                optionCerrar();
                break;
            case 1: //INFO
                optionInfo();
                optionCerrar();
                break;
            case 2: //CERRAR - Cierra el SubMenu y vuelve a activar la seleccion en la tienda
                optionCerrar();
                break;
        }
    }

    public void optionComprar() {
        itemToBuy = tienda.index;
        playerInventory.addItem(tiendaInventory.items[itemToBuy]);    //AÑADE EL ITEM AL INVENTARIO DEL JUGADOR
    }
    public void optionVender() {
        itemToBuy = tienda.index;
        playerInventory.removeItem(playerInventory.items[itemToBuy]);    //AÑADE EL ITEM AL INVENTARIO DEL JUGADOR
        menusManager.actualizarInventarioTienda();
    }
    public void optionInfo() {
        print("NOMBRE ITEM: " + tiendaInventory.items[itemToBuy].name);
    }
    public void optionCerrar() {
        this.transform.parent.gameObject.SetActive(false);
        tienda.selecting = false;
    }

}
