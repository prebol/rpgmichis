using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class T_MM_Group: MonoBehaviour
{
    [SerializeField] private CanvasRenderer comprarGUI, venderGUI, allTienda, itemsContainer;
    [SerializeField] private GameObject michi;

    [SerializeField] private MenusManager menuManager;
    public List<CanvasRenderer> tabButtons;

    [SerializeField] public bool hasSelected = false;

    public int index = 0;  //NUM ELEMENTO EN EL QUE ESTA EL JUGADOR EN ESTE MENU

    public void Start()
    {
        menuManager = menuManager.GetComponent<MenusManager>();
    }

    public void OnTabEnter(T_MM_Button button)
    {
        
 
    }

    public void OnTabExit(T_MM_Button button)
    {
        
    }

    public void OnTabSelected(T_MM_Button button)
    {
        
    }

    void OnEnable() {
        index = 0;  //REINICIA PARA QUE SIEMPRE ESTE SELECCIONADA LA PRIMERA OPCION AL PRINCIPIO
        hasSelected = false;
    }

    void Update() {

        if (!hasSelected) {
            moveThrowItems();

            //SELECCIONAR OPCION
            if (Input.GetKeyDown(KeyCode.Return)) {
                optionSelected();
            }
            if (Input.GetKeyDown(KeyCode.Backspace)) {
                menuManager.ocultarMainMenu();
            }
        }

    }
    
    public void toggleSelected() {
        hasSelected = !hasSelected;
    }

    //PARA CAMBIAR ENTRE LAS OPCIONES DEL MENU + 'HOVER'
    public void moveThrowItems() {

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
            index--;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {
            index++;
        }

        //print($"index: {index}");
        int len = tabButtons.Count;
        if (index < 0) index = 0;
        if (index >= len) index = len-1;
        for (int i = 0;i < len;i++) {
            if (i == index) {
                tabButtons[i].GetComponent<Image>().color = new Color(0.5754717f, 0.5754717f, 0.5754717f);  //HACE HIGHLIGHT EN EL ITEM EN EL QUE ESTAS
            } else {
                tabButtons[i].GetComponent<Image>().color = new Color(0, 0, 0); //EL RESTO LOS PONE EN COLOR DEFAULT
            }
            //print($"i: {i}, button: {tabButtons[i].transform.name}");
        }
    }

    //GESTIONA LA ACCION QUE HACE CUANDO LE DAS A UNO DE LOS BOTONES
    public void optionSelected() {
        hasSelected = true;
        switch(index) {
            case 0: //COMPRAR - Comprar, carga la GUI para comprar Items de la tienda
                menuManager.mostrarShopWindow("comprar");
                break;
            case 1: //VENDER - Vender, carga la GUI para vender Items de tu inventario
                menuManager.mostrarShopWindow("vender");
                break;
            case 2: //CERRAR - Cierra el MainMenu
                menuManager.ocultarMainMenu();
                break;
        }
    }


}
