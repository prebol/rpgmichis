    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movPrueba : MonoBehaviour
{
    Rigidbody2D rb;
    [SerializeField]
    private Camera cam;
    [SerializeField] private CanvasRenderer invContainer;
    public int spd = 3;

    [SerializeField]
    private InventoryController inventory;
    // Start is called before the first frame update

    [SerializeField] private RPGPotion[] tipoPociones;
    /*
        //TODOS LOS TIPOS DE 'POCIONES' QUE HAY
        [0] -> Revivir (pluma)
        [1] -> HP Potion
        [2] -> MP Potion
    */
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        transform.position = cam.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W)) {
            rb.velocity = new Vector2(rb.velocity.x, spd);
        } else if (Input.GetKey(KeyCode.S)) {
            rb.velocity = new Vector2(rb.velocity.x, -spd);
        } else {
            rb.velocity = new Vector2(rb.velocity.x, 0);
        }

        if (Input.GetKey(KeyCode.D)) {
            rb.velocity = new Vector2(spd, rb.velocity.y);
        } else if (Input.GetKey(KeyCode.A)) {
            rb.velocity = new Vector2(-spd, rb.velocity.y);
        } else {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.T)) {
            if (invContainer.gameObject.activeSelf) {
                invContainer.gameObject.SetActive(false);
            } else {
                invContainer.gameObject.SetActive(true);
                invContainer.GetComponent<LoadInventory>().loadInventory();
            }
        }

        if (Input.GetKeyDown(KeyCode.P)) {
            RPGPotion item = RPGPotion.CreateInstance<RPGPotion>();
            item = tipoPociones[0]; //SETEAMOS EL SCRIPTABLE PERTENIENTE
            inventory.addItem(item);
        }
    }

}
