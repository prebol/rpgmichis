using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{

    [SerializeField] private int maxItems = 32;
    public List<RPGItem> items = new List<RPGItem>();

    public void addItem(RPGItem i)
    {
        if (items.Count < maxItems) {
            items.Add(i);
        } else {
            print("YA SE HA ALCANZADO EL LIMITE MAXIMO DE ITEMS DEL INVENTARIO");
        }
        
    }

    public void removeItem(RPGItem i) {
        items.Remove(i);
    }
}
