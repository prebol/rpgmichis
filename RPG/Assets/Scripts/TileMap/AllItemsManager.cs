using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllItemsManager : MonoBehaviour
{

    public static AllItemsManager Instance;

    void Awake()
    {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }
    
    //-----POCIONES-----//
    [Header("Potion's Scriptable")]
    public RPGPotion revivir;
    public RPGPotion hpPotion;
    public RPGPotion mpPotion;
    public RPGPotion vitaminaC;

    [Space(10)]

    //-----ARMAS-----//
    [Header(header: "Weapon's Scriptable")]

    //---ARCOS---//
    [Header("ARCOS")]
    public RPGWeapon arcoBueno;
    public RPGWeapon arcoFuego;
    public RPGWeapon arcoHielo;
    public RPGWeapon arcoMadera;
    public RPGWeapon arcoRayo;

    //---DAGES---//
    [Header("DAGAS")]
    public RPGWeapon dagaClasica;
    public RPGWeapon dagaMadera;
    public RPGWeapon katana;
    public RPGWeapon masamune;

    //---HEAVY SWORD---//
    [Header("ESPADAS PESADAS")]
    public RPGWeapon claymoreDorada;
    public RPGWeapon claymoreNormal;
    public RPGWeapon espadonMadera;
    public RPGWeapon filoInfinito;

    //---STAFF---//
    [Header("BACULOS")]
    public RPGWeapon baculoMadera;
    public RPGWeapon varaFuego;
    public RPGWeapon varaHielo;
    public RPGWeapon varaRayo;
    public RPGWeapon varitaMagica;

    //---SWORD AND SHIELD---//
    [Header("ESPADA Y ESCUDO")]
    public RPGWeapon espadaMaderaEscudo;

    [Space(10)]

    //-----ARMADURAS-----//
    [Header("Armor's Scriptable")]

    //---MICHI---//
    [Header("Michi Armor")]
    [Header("Charm")]
    public RPGArmor plumaDesgastada;
    public RPGArmor plumaCazador;
    public RPGArmor plumaFurtiva;
    public RPGArmor plumaVerde;
    public RPGArmor plumaNegra;
    [Header(header: "Greaves")]
    public RPGArmor pantalonesRotos;
    public RPGArmor pantalonesCuero;
    public RPGArmor tabisFurtivas;
    public RPGArmor pantalonesVerdes;
    public RPGArmor botasNoche;
    [Header(header: "Helmet")]
    public RPGArmor veloAntiguo;
    public RPGArmor capuchaCuero;
    public RPGArmor mascaraFurtiva;
    public RPGArmor capuchaVerde;
    public RPGArmor veloNoche;
    [Header(header: "Plate")]
    public RPGArmor ropajesAntiguos;
    public RPGArmor tunicaCuero;
    public RPGArmor ponchoFurtivo;
    public RPGArmor tunicaVerde;
    public RPGArmor capaNoche;
    //-----------//
    //---NUTRIA---//
    [Header("Nutria Armor")]
    [Header("Charm")]
    public RPGArmor cuerdaRota;
    public RPGArmor cuerdaNormal;
    public RPGArmor cuerdaGruesa;
    public RPGArmor cuerdaDorada;
    public RPGArmor hiloDivino;
    [Header(header: "Greaves")]
    public RPGArmor mallasOxidadas;
    public RPGArmor mallasNormales;
    public RPGArmor mallasFurtivas;
    public RPGArmor mallasOscuras;
    public RPGArmor mallasDivinas;
    [Header(header: "Helmet")]
    public RPGArmor capuchaRoja;
    public RPGArmor cascoGuerrero;
    public RPGArmor antifazFurtivo;
    public RPGArmor capuchaOscura;
    public RPGArmor antifazDivino;
    [Header(header: "Plate")]
    public RPGArmor cotaOxidada;
    public RPGArmor cotaMalla;
    public RPGArmor tunicaFurtiva;
    public RPGArmor tunicaNegra;
    public RPGArmor capaDivina;
    //-----------//
    //---PERRO---//
    [Header("Perro Armor")]
    [Header("Charm")]
    public RPGArmor huesoRoto;
    public RPGArmor huesoNormal;
    public RPGArmor huesoOro;
    public RPGArmor huesoPlatino;
    public RPGArmor huesoDiamante;
    [Header(header: "Greaves")]
    public RPGArmor grebasDesgastadas;
    public RPGArmor mallasAcero;
    public RPGArmor grebasOro;
    public RPGArmor mallasPlatino;
    public RPGArmor mallasDiamante;
    [Header(header: "Helmet")]
    public RPGArmor cascoRoto;
    public RPGArmor cascoAcero;
    public RPGArmor cascoOro;
    public RPGArmor yelmoPlatino;
    public RPGArmor yelmoDiamante;
    [Header(header: "Plate")]
    public RPGArmor corazaOxidada;
    public RPGArmor placaAcero;
    public RPGArmor corazaOro;
    public RPGArmor placaPlatino;
    public RPGArmor corazaDiamante;
    //-----------//
    //---PINGUINO---//
    [Header("Pinguino Armor")]
    [Header("Charm")]
    public RPGArmor gemaApagada;
    public RPGArmor gemaAvivadora;
    public RPGArmor gemaPoderosa;
    public RPGArmor gemaOscura;
    public RPGArmor gemaInfinita;
    [Header(header: "Greaves")]
    public RPGArmor taparrabosRoto;
    public RPGArmor faldaLigera;
    public RPGArmor cinturonSellado;
    public RPGArmor taparrabosMagico;
    public RPGArmor faldaEmperatriz;
    [Header(header: "Helmet")]
    public RPGArmor diademaRota;
    public RPGArmor diademaLigera;
    public RPGArmor diademaSellada;
    public RPGArmor felpaMagica;
    public RPGArmor coronaEmperatriz;
    [Header(header: "Plate")]
    public RPGArmor tunicaRota;
    public RPGArmor vestidoLigero;
    public RPGArmor tunicaSellada;
    public RPGArmor tunicaMagica;
    public RPGArmor vestidoEmperatriz;
    //-----------//

}
