using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interaccion : MonoBehaviour
{
    void Start() {
    }
    public virtual bool interactuar() {
        return true;
    }

    public GameObject getIndicador() {
        return transform.Find("F").gameObject;
    }
}
