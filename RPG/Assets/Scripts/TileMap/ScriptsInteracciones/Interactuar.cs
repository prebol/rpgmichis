using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactuar : MonoBehaviour
{
    private GameObject objInteraccion;

    void Update() {

        if (Input.GetKeyDown(KeyCode.F) && this.GetComponent<Movement>().canMove) {
            print("Intentando interactuar...");
            if (objInteraccion != null) {
                bool interactuado = objInteraccion.GetComponent<Interaccion>().interactuar();
            } else {
                print("No hay nada con que interactuar...");
            }
            
        }
    }
    
    void OnTriggerEnter2D(Collider2D collision) {
        //  print("Trigger Enter 2D");
         if (collision.transform.CompareTag("Interaccion")) {
            objInteraccion = collision.gameObject;
            objInteraccion.GetComponent<Interaccion>().getIndicador().SetActive(true);
         }
        
         if (collision.transform.CompareTag("FronteraCastillo")) {
            StartCoroutine(cambiarScene("Mapa_Castillo", 5.53f, -25f));
            ChangeSceneManager.Instance.activarTransicion();
         }
         if (collision.transform.CompareTag("FronteraNieve")) {
            StartCoroutine(cambiarScene("Mapa_Nieve", 3.47f, 25.6f));
            ChangeSceneManager.Instance.activarTransicion();
         }
     }

     IEnumerator cambiarScene(string sceneName, float x, float y) {
        yield return new WaitForSeconds(1.5f);
        ChangeSceneManager.Instance.cargarScene(sceneName);
        GameObject.Find("Michi").GetComponent<Movement>().changePosition(x, y);
    }

     void OnTriggerExit2D(Collider2D collision) {
        //  print("Trigger Exit 2D");
         if (collision.transform.CompareTag("Interaccion")) {
            objInteraccion.GetComponent<Interaccion>().getIndicador().SetActive(false);
            objInteraccion = null;
         }
     }
}
