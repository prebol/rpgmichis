using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySelect : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public CanvasRenderer selector;
    private GameManager_Combate GM;
    public List<TabEnemy> tabButtons;
    public int Distx;
    public int index = 0;
    private int numColumns = 6;

    public void Start()
    {
        this.GM = GameObject.Find("GameManager").GetComponent<GameManager_Combate>();
        foreach (GameObject party in GM.PartyCombate)
        {

            party.GetComponent<PartyCombate>().Ens = this;


        }
    }

    private void OnEnable()
    {
        
    }

    public void Subscribe(TabEnemy button)
    {
        if (tabButtons == null)
        {
            tabButtons = new List<TabEnemy>();
        }

        tabButtons.Add(button);
    }

    public void OnTabEnter(TabEnemy button)
    {


    }

    public void OnTabExit(TabEnemy button)
    {

    }

    public void OnTabSelected(TabEnemy button)
    {
        foreach (TabEnemy tb in tabButtons)
        {
            char num = tb.transform.name[tb.transform.name.Length - 1];
            print(num);
        }
    }

    void Update()
    {
        if (selector.gameObject.active) 
        {
            if (GM.boss)
            {
                this.index = 1;
            }
            else
            {
                controlSeleccion();
            }
        }
        
        //print(index);

    }

    private void controlSeleccion()
    {

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {      //ARRIBA
            index--;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {      //ABAJO
            index++;
        }

        int len = tabButtons.Count;
        //print(len);
        if (index < 0) index = 0;
        if (index >= len) index = len - 1; 

        selector.transform.position = new Vector3(transform.GetChild(index).position.x - Distx, transform.GetChild(index).position.y, 0);
    }
     
    
}
