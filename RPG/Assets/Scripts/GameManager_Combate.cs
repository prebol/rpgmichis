using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class GameManager_Combate : MonoBehaviour
{
    public AudioClip clip;
    public ActiveParty ActiveParty;
    public List<PartyMember> Party;
    public GameObject[] PartyCombate = new GameObject[4];
    public List<Enemy> ListaEnemies;
    public GameObject[] Enemies = new GameObject[3];
    public Queue<Bicho> turnos = new Queue<Bicho>();
    public float expTotal = 0;    
    private bool enTurno = false; 
    public CanvasRenderer panel;
    public CanvasRenderer attackNotify;
    public GameObject numero;
    public TextMeshProUGUI[] movs = new TextMeshProUGUI[4];
    public int control=0;
    public TabEnemy[] tabs = new TabEnemy[3];
    public TabAlly[] tabsAl = new TabAlly[3];
    public bool boss;
    private bool nextAttack = false;
    private int ally = 0;
    private int enemy = 0;
    public int enemigosmax=3;


    void Start() 
    {
        SoundManager.Instance.PlayMusic(clip);

        Party.Add(ActiveParty.Michi);

        PartyCombate[control].GetComponent<PartyCombate>().PartyMember = Party[control];
        PartyCombate[control].SetActive(true);
        control++;
        ally++;
        if (ActiveParty.Perro != null)
        {
            Party.Add(ActiveParty.Perro);
            PartyCombate[control].GetComponent<PartyCombate>().PartyMember = Party[control];
            if (PartyCombate[control].GetComponent<PartyCombate>().PartyMember.dead != true)
            {
                PartyCombate[control].gameObject.SetActive(true);
            }
            ally++;
            control++;
        }
        if (ActiveParty.Nutria != null)
        {
            Party.Add(ActiveParty.Nutria);
            PartyCombate[control].GetComponent<PartyCombate>().PartyMember = Party[control];
            if (PartyCombate[control].GetComponent<PartyCombate>().PartyMember.dead != true)
            {
                PartyCombate[control].gameObject.SetActive(true);
            }
            ally++;
            control++;
        }
        if (ActiveParty.Pinguino != null)
        {
            Party.Add(ActiveParty.Pinguino);
            PartyCombate[control].GetComponent<PartyCombate>().PartyMember = Party[control];
            if (PartyCombate[control].GetComponent<PartyCombate>().PartyMember.dead != true)
            {
                PartyCombate[control].gameObject.SetActive(true);
            }
            ally++;
            control++;
        }

        if (boss)
        {
            enemy++;
            Enemies[1].SetActive(true);
            Enemies[1].GetComponent<EnemyIA>().enemyinfo = ListaEnemies[0];
        }
        else
        {
            Random r = new Random();

            for (int i = 0; i < enemigosmax; i++)
            {
                enemy++;
                Enemies[i].SetActive(true);  
                Enemies[i].GetComponent<EnemyIA>().enemyinfo = ListaEnemies[r.Next(ListaEnemies.Count)];
            }
            for (int i = 0; i < 3; i++)
            {
                if (!Enemies[i].active)
                {
                    Destroy(Enemies[i].gameObject);

                };

            }
        }
    }

    void Update()
    {
        if (!enTurno)
        {
            if (turnos.Count != 0)
            { 
                Turn();
            }
        } 
    }

    public void Turn()
    {
        enTurno = true;
        Bicho b = turnos.Dequeue();
        if (b != null) 
        {
            if (b.GetType() == typeof(PartyCombate))
            {
                b.GetComponent<PartyCombate>().EligiendoAtaque();
                panel.gameObject.SetActive(true);
                for (int i = 0; i < b.GetComponent<PartyCombate>().PartyMember.attacks.Count; i++)
                {
                    movs[i].text = b.GetComponent<PartyCombate>().PartyMember.attacks[i].name;
                }
            }
            else if (b.GetType() == typeof(EnemyIA))
            {
                if (this.ally != 0)
                {
                    b.GetComponent<EnemyIA>().DoAttack();
                }
            } 
        }
    }

    public void endTurn()
    { 
        enTurno = false; 
        panel.gameObject.SetActive(false);   
    }
      
    public void endCombate()
    {
        StopCoroutine(AttackNotifyDisable());
        for (int i = 0; i < PartyCombate.Length; i++)
        {
            if (PartyCombate[i].activeSelf)
            {
                PartyCombate[i].GetComponent<PartyCombate>().EndCombate(this.expTotal);
            }
        }
        this.attackNotify.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "�La party ha ganado el combate!";
        this.attackNotify.gameObject.SetActive(true);
        StartCoroutine(Exit());
    }
     
    IEnumerator Exit()
    {
        yield return new WaitForSeconds(3f);

        //StartCoroutine(GameObject.FindObjectOfType<SceneFader>().FadeAndLoadScene(SceneFader.FadeDirection.In,"PruebasJuan"));
        ChangeSceneManager.Instance.cargarScene("Mapa_Nieve");
        //SceneManager.LoadScene("Mapa_Nieve");
    }

    public void DeleteObjective(EnemyIA dead)
    {
        dead.alive = false;
        Destroy(dead.gameObject);
        enemy--;
        for (int i = 0; i < Enemies.Length; i++)
        { 
            if (Enemies[i] == dead)
            {
                Enemies[i] = null;
            }
        }

        bool correcto = false;

        while (!correcto)
        {
            correcto = true;
            for (int i = 0; i < Enemies.Length; i++)
            {
                if (i + 1 < Enemies.Length)
                {
                    if (Enemies[i] == null && Enemies[i + 1] != null)
                    {
                        Enemies[i] = Enemies[i + 1];
                        Enemies[i + 1] = null;
                        correcto = false;
                    }
                }
            }
        }

        if (enemy == 0)
        {
            this.endCombate();
        }

    }

    public void AllyDown()
    {
        this.ally--;
        if (this.ally == 0)
        {
            StopCoroutine(AttackNotifyDisable());
            this.attackNotify.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "La party ha ca�do...";
            this.attackNotify.gameObject.SetActive(true);
            StartCoroutine(ExitLose());
        }
    }

    IEnumerator ExitLose()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("GameOver");
    }

    public void NotifyAttack(Attacks attack, string attacker, string defender, float dmg, float x, float y, bool dodged)
    {
        int extra = (1 * 105);
        x *= 105;
        x += extra;
        y *= 105; 
        dmg = Mathf.Round(dmg); 
        if (dodged)
        {
            this.numero.GetComponent<TextMeshProUGUI>().text = "DODGED!";
            this.numero.transform.position = new Vector3(x + 960, y + 540, 0);
            this.numero.gameObject.SetActive(true); 
        }
        if (attack.objective == Objective.Aliado)
        {
            this.numero.GetComponent<TextMeshProUGUI>().text = dmg + "";
            this.numero.transform.position = new Vector3(x+960, y+540, 0);
            this.numero.gameObject.SetActive(true);
        } 
        else if (attack.objective == Objective.Enemigo)
        {
            this.numero.GetComponent<TextMeshProUGUI>().text = -dmg+"";
            this.numero.transform.position = new Vector3(x+960, y+540, 0);
            this.numero.gameObject.SetActive(true); 
        }
        if (attackNotify.gameObject.activeSelf)
        {
            nextAttack = true;
        }
        this.attackNotify.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "�" + attacker + " ha utilizado " + attack.name + " con " + defender + "!";
        this.attackNotify.gameObject.SetActive(true);
        StartCoroutine(AttackNotifyDisable());
    }

    public void NotifyAttackEnemy(Attacks attack, string attacker, string defender, float dmg, float x, float y, bool dodged)
    {
        int extra = (1 * 105);
        x *= 105;
        x += extra;
        y *= 105;
        dmg = Mathf.Round(dmg);

        if (dodged)
        {
            this.numero.GetComponent<TextMeshProUGUI>().text = "DODGED!";
            this.numero.transform.position = new Vector3(x + 960, y + 540, 0);
            this.numero.gameObject.SetActive(true);
        }
        else
        {
            if (attack.type != AttackType.Healing)
            {
                this.numero.GetComponent<TextMeshProUGUI>().text = -dmg + "";
            }
            else
            {
                this.numero.GetComponent<TextMeshProUGUI>().text = "+"+dmg;
            }
            this.numero.transform.position = new Vector3(x + 960, y + 540, 0);
            this.numero.gameObject.SetActive(true);
        }
        if (attackNotify.gameObject.activeSelf)
        {
            nextAttack = true;
        }
        this.attackNotify.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "�" + attacker + " ha utilizado " + attack.name + " con " + defender + "!";
        this.attackNotify.gameObject.SetActive(true);
        StartCoroutine(AttackNotifyDisable());
    }

    public void chargedTurn(Bicho bicho) 
    { 
        this.turnos.Enqueue(bicho);
    }
      
    IEnumerator AttackNotifyDisable() 
    {
        yield return new WaitForSeconds(2f);
        if (!nextAttack)
        {
            this.numero.gameObject.SetActive(false);
            this.attackNotify.gameObject.SetActive(false);
        }
        else
        {
            this.nextAttack = false;
        }
    } 
     
    /* 
    public void initializePartyMember(int i)
    {
        print(PartyCombate[i].GetComponent<PartyCombate>().GameManager);
        PartyCombate[i].GetComponent<PartyCombate>().GameManager = this.gameObject;
        print(PartyCombate[i].GetComponent<PartyCombate>().GameManager);
    }*/

}
