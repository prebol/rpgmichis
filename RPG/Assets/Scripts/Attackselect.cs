using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attackselect : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private CanvasRenderer selector;
    private GameManager_Combate GM;
    public List<TabAttack> tabButtons;
    public int Distx;
    public int index = 0; 
    private int numColumns = 6;

    public void Start() 
    {

    }

    private void OnEnable()
    {
        this.GM = GameObject.Find("GameManager").GetComponent<GameManager_Combate>();
        foreach(GameObject party in GM.PartyCombate)
        {
            
                party.GetComponent<PartyCombate>().att = this;
                
                 
        }
    }

    public void Subscribe(TabAttack button)
    {
        if (tabButtons == null)
        { 
            tabButtons = new List<TabAttack>();
        }

        tabButtons.Add(button);
    } 

    public void OnTabEnter(TabAttack button)
    {


    }

    public void OnTabExit(TabAttack button)
    {

    }

    public void OnTabSelected(TabAttack button)
    {
        foreach (TabAttack tb in tabButtons)
        {
            char num = tb.transform.name[tb.transform.name.Length - 1];
            print(num);
        }
    }

    void Update()
    {

        controlSeleccion();
        //print(index);

    }

    private void controlSeleccion()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {      //ARRIBA
            index--;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {      //ABAJO
            index++;
        }

        int len = tabButtons.Count;
        //print(len);
        if (index < 0) index = 0;
        if (index >= len) index = len - 1;

        selector.transform.position = new Vector3(transform.GetChild(index).position.x-Distx, transform.GetChild(index).position.y, 0);
    }
}
