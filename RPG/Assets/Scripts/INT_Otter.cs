using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class INT_Otter : Interaccion
{
    // Start is called before the first frame update
    public PartyMember nutria;
    GameManager gM;
    public string textToDisplay;
    public int tamanoTxt;
    public bool progresivo;
    public float velocidadTxt;
    private CanvasRenderer msgCanvas;
    private TextMeshProUGUI msgTxt;
    private GameObject michi;

    private void Start()
    {
        michi = GameObject.Find("Michi");
        msgCanvas = transform.Find("Canvas").transform.Find("msg").GetComponent<CanvasRenderer>();
        msgTxt = msgCanvas.transform.Find("msgTxt").GetComponent<TextMeshProUGUI>();

        msgTxt.fontSize = tamanoTxt;

        gM = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (gM.Story.nutria)
        {
            Destroy(this.gameObject);
        }  
    }

    public override bool interactuar()
    {
        print("sazfdsafdsdf");
        if (!gM.Story.quest0)  
        {   
            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Dejame cacho mierda";
            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else   
            {  
                mostrarCartelInsta();   
            }

            return true;
        }
        else if (gM.Story.perroJoined&&!gM.Story.nutria)
        {
            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Hombre, parece que conoces a Dogo, me unire a vosotros para esta tremenda aventura. Vayamos al castillo al norte del pueblo";

            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else
            {
                mostrarCartelInsta();
            }

            return true;

        }
        return true;

    }  

    public bool cartelOpen = false;



    void Update()
    {
        if (cartelOpen)
        {
            if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape))
            {
                ocultarCartel();
            }
        }

    }



    IEnumerator mostrarCartelProgresivo()
    {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = "";
        foreach (char c in textToDisplay.ToCharArray())
        {
            msgTxt.text += c;
            yield return new WaitForSeconds(velocidadTxt);
        }
    }

    public void mostrarCartelInsta()
    {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = textToDisplay;
    }

    public void ocultarCartel()
    {
        StopAllCoroutines();
        michi.GetComponent<Movement>().canMove = true;
        msgCanvas.gameObject.SetActive(false);
        cartelOpen = false;
        if (!gM.Story.quest0)
        {
            gM.Story.quest0 = true;
            SceneManager.LoadScene("Combate");
        }
        else if (gM.Story.perroJoined && !gM.Story.nutria)
        {
            gM.Story.nutria = true;
            gM.ActiveParty.Nutria = nutria;
            Destroy(this.gameObject);

        }

    } 
}
        