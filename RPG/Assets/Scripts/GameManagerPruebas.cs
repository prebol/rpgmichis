using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
public class GameManagerPruebas : MonoBehaviour  
{

    public List<Enemy> ListaEnemies;
    public GameObject[] enemies = new GameObject[3];
       

    // Start is called before the first frame update
    void Start()
    {  
        Random r = new Random();
        
        for (int i = 0; i < r.Next(3)+1; i++) 
        {  
            enemies[i].SetActive(true);
            enemies[i].GetComponent<EnemyIA>().enemyinfo = ListaEnemies[r.Next(ListaEnemies.Count)];
        }
        
    }

    // Update is called once per frame
    void Update()
    {  
        
    }
}  
