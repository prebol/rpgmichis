using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Story : ScriptableObject
{
    // Start is called before the first frame update
    public bool quest0;
    public bool perroJoined;
    public bool nutria;
    public bool pinguino;
    public bool boss;

}
