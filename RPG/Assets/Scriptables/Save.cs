using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Save : ScriptableObject
{
    public Story story;
    public ActiveParty party;
    public Position position;
    public Inventory inventory;
}
 