using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Inventory : ScriptableObject
{
    public int maxItems = 32;
    public List<RPGItem> items = new List<RPGItem>();
}
