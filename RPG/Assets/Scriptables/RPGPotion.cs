using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RPGPotion : RPGItem
{
    public TipoPocion tipo;
}

public enum TipoPocion
{
    HP,
    MANA,
    REVIVIR
}
