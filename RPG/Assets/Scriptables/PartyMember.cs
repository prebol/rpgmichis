using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu]
public class PartyMember : ScriptableObject
{
    public Sprite sprite;
    public float BaseHP;
    public float MaxHP;
    public float HP;
    public float BaseMP;
    public float MaxMP;
    public float MP;
    public float BaseAtk;
    public float Atk;
    public float BaseAP;
    public float Ap;
    public float BaseArmor;
    public float Armor;
    public float BaseMagicResistance;
    public float MagicResistance;
    public float Spd;
    public float BaseDodge;
    public float Dodge;
    public bool dead;
    public TipoWeapon Type;
    public GameObject gameObject;

    //Equipment[0]=>Helmet, Equipment[1]=> Plate, Equipment[2] =>Greaves, Equipment[3] =>Charm, Equipment[4] =>Weapon.
    public List<RPGItem> Equipment;

    //4 attacks
    public List<Attacks> attacks;

    public float level=1;
    public float exp=0;
    public float limitExp = 100;

    public void ChangeEquipment(RPGItem equipment)
    {
        //RPGItem oldItem = null;

        if (equipment.GetType() == typeof (RPGWeapon))
        {
            //oldItem = this.Equipment[4];
            this.Equipment[4] = (RPGWeapon) equipment;
        }
        else if (equipment.GetType() == typeof (RPGArmor))
        {
            int x = 0;
            RPGArmor newArmor = (RPGArmor)equipment;
            if (newArmor.parte == TipoArmor.HELMET)
            {
                x = 0;
            }
            else if (newArmor.parte == TipoArmor.PLATE)
            {
                x = 1;
            }
            else if (newArmor.parte == TipoArmor.GREAVES)
            {
                x = 2;
            }
            else if (newArmor.parte == TipoArmor.CHARM)
            {
                x = 3;
            }
            //oldItem = this.Equipment[x];
            this.Equipment[x] = newArmor;
        }
        this.ChangeStats();
        
        //return oldItem;
    }
     
    public void AddExp(float addExp) 
    {
        this.exp += addExp;
        if (this.exp >= this.limitExp)
        {
            this.LevelUp();
        }
    }
    
    void LevelUp()
    {
        this.level++;
        this.limitExp *= 2f;
        this.exp = 0;
        this.HP = this.MaxHP;
        this.ChangeStats(); 
    }
     
    public void ChangeStats()
    {
        CalculoAD();  
        CalculoAP();
        CalculoMR();
        CalculoArmor();
        CalculoDodge();
        CalculoHP();
        CalculoMP();
    }

    void CalculoHP()
    {
        RPGArmor helmet = (RPGArmor)this.Equipment[0];
        RPGArmor plate = (RPGArmor)this.Equipment[1];
        RPGArmor greaves = (RPGArmor)this.Equipment[2]; 
        RPGArmor charm = (RPGArmor)this.Equipment[3];
        this.MaxHP = this.BaseHP + helmet.hp + plate.hp + greaves.hp + charm.hp + ((this.BaseHP / 10) * (level-1));
    }

    void CalculoMP()
    {
        RPGArmor helmet = (RPGArmor)this.Equipment[0];
        RPGArmor plate = (RPGArmor)this.Equipment[1];
        RPGArmor greaves = (RPGArmor)this.Equipment[2];
        RPGArmor charm = (RPGArmor)this.Equipment[3];
        this.MaxMP = this.BaseMP + helmet.mp + plate.mp + greaves.mp + charm.mp + ((this.BaseMP / 10) * (level - 1));
    }
     
    void CalculoArmor()
    {
        RPGArmor helmet = (RPGArmor)this.Equipment[0];
        RPGArmor plate = (RPGArmor)this.Equipment[1];
        RPGArmor greaves = (RPGArmor)this.Equipment[2];
        RPGArmor charm = (RPGArmor)this.Equipment[3];
        this.Armor = this.BaseArmor + helmet.armor + plate.armor + greaves.armor + charm.armor + ((this.BaseArmor / 10) * (level - 1));
    }

    void CalculoMR()
    {
        RPGArmor helmet = (RPGArmor)this.Equipment[0];
        RPGArmor plate = (RPGArmor)this.Equipment[1];
        RPGArmor greaves = (RPGArmor)this.Equipment[2];
        RPGArmor charm = (RPGArmor)this.Equipment[3];
        this.MagicResistance = this.BaseMagicResistance + helmet.mr + plate.mr + greaves.mr + charm.mr + ((this.BaseHP / 10) * (level - 1));
    }

    void CalculoDodge()
    {
        RPGArmor helmet = (RPGArmor)this.Equipment[0];
        RPGArmor plate = (RPGArmor)this.Equipment[1];
        RPGArmor greaves = (RPGArmor)this.Equipment[2];
        RPGArmor charm = (RPGArmor)this.Equipment[3];
        this.Dodge = this.BaseDodge + helmet.dodge + plate.dodge + greaves.dodge + charm.dodge + ((this.BaseDodge / 10) * (level - 1));
    }

    void CalculoAD()
    {
        RPGWeapon weapon = (RPGWeapon)this.Equipment[4];
        this.Atk = this.BaseAtk + weapon.atk + ((this.BaseHP / 10) * (level - 1));
    }

    void CalculoAP()
    {
        RPGWeapon weapon = (RPGWeapon)this.Equipment[4];
        this.Ap = this.BaseAP + weapon.ap + ((this.BaseAP / 10) * (level - 1));
    }

}

