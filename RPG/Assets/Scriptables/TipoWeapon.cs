using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipoWeapon
{
    HEAVYSWORD,
    SWORDANDSHIELD,
    DAGE,
    BOW,
    STAFF
}
