using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ActiveParty : ScriptableObject
{
    public PartyMember Michi;
    public PartyMember Perro;
    public PartyMember Nutria;
    public PartyMember Pinguino;
}
