using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RPGWeapon : RPGItem
{
    public float atk;
    public float ap;
    public Elemento elem;
    public TipoWeapon tipo;
}
