using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPGItem : ScriptableObject
{
    public Sprite sprite;
    public string name;
    public int value;
}

public enum Elemento
{
    NONE,
    FIRE,
    ICE,
    LIGHTNING
}


