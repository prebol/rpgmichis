using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Attacks : ScriptableObject
{
    public string name;
    public Affections afecction;
    public float costMana;
    public float damage;
    public Elemento Elemento;
    public int probability;
    public AttackType type;
    public Objective objective;
    public Target target;

    //Effects
    public int addAtk;
    public int addAp;
    public int addArmor;
    public int addMr;
    public int addSpd;
    public int addDodge;


}
public enum Affections 
{

    Poison, Freeze, Burn, None

}
public enum AttackType 
{
    Magical,Physical,Healing,Else
}

public enum Objective
{
    Aliado,Enemigo
}

public enum Target
{
    Mono,Multi
}
