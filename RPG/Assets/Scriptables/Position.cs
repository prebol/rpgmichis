using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Position : ScriptableObject
{
    public Vector3 position;
    public string scene;
}
