using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Enemy : ScriptableObject
{
    public Sprite sprite;
    public float MaxHP;
    public float HP;
    public float Ap;
    public float Atk;
    public float Magic_armor;
    public float Phisyc_armor;
    public float Velociy;
    public float Dodge;
    public float exp;
    public bool boss;

    public List<RPGItem> loot;
    public int tcarga;  
    public List<Attacks> attacks;  

}