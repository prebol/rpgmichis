using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RPGArmor : RPGItem
{
    public float hp;
    public float mp;
    public float armor;
    public float mr;
    public float dodge;
    public TipoWeapon estilo;
    public TipoArmor parte;
}

public enum TipoArmor
{
    HELMET,
    PLATE,
    GREAVES, 
    CHARM
}
