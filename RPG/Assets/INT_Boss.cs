using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class INT_Boss : Interaccion
{
    // Start is called before the first frame update
    public PartyMember pingu;
    GameManager gM;
    public string textToDisplay;
    public int tama�oTxt;
    public bool progresivo;
    public float velocidadTxt;
    private CanvasRenderer msgCanvas;
    private TextMeshProUGUI msgTxt;
    private GameObject michi;

    private void Start()
    {
        michi = GameObject.Find("Michi");
        msgCanvas = transform.Find("Canvas").transform.Find("msg").GetComponent<CanvasRenderer>();
        msgTxt = msgCanvas.transform.Find("msgTxt").GetComponent<TextMeshProUGUI>();

        msgTxt.fontSize = tama�oTxt;

        gM = GameObject.Find("GameManager").GetComponent<GameManager>();
        
    }

    public override bool interactuar()
    {

        if (gM.Story.pinguino && !gM.Story.boss)
        {
            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Que empieze la batalla pedazo de escoria";

            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else
            {
                mostrarCartelInsta();
            }
            
            return true;

        }
       
        else if (!gM.Story.nutria&&gM.Story.nutria&&!gM.Story.boss)
        {

            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Creeis que podreis contra mi? Intentadlo pero sois menos de los que esperaba";

            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else
            {
                mostrarCartelInsta();
            }

            return true;
        }
        else 
        {

            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Quienes os crees que sois, largo de aqui";

            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else
            {
                mostrarCartelInsta();
            }

            return true;
        }
        return true;
    }

    public bool cartelOpen = false;



    void Update()
    {
        if (cartelOpen)
        {
            if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape))
            {
                ocultarCartel();
            }
        }

    }



    IEnumerator mostrarCartelProgresivo()
    {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = "";
        foreach (char c in textToDisplay.ToCharArray())
        {
            msgTxt.text += c;
            yield return new WaitForSeconds(velocidadTxt);
        }
    }

    public void mostrarCartelInsta()
    {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = textToDisplay;
    }

    public void ocultarCartel()
    {
        StopAllCoroutines();
        michi.GetComponent<Movement>().canMove = true;
        msgCanvas.gameObject.SetActive(false);
        cartelOpen = false;



        if (gM.Story.pinguino && !gM.Story.boss)
        {

            gM.Story.pinguino = true;

            gM.ActiveParty.Pinguino = pingu;

            SceneManager.LoadScene("CombateMap_Boss");

        }
        else if (gM.Story.nutria && !gM.Story.pinguino)
        {

            gM.Story.pinguino = true;

            gM.ActiveParty.Pinguino = pingu;
            
            SceneManager.LoadScene("CombateMap_Boss");
        }
        else 
        {
            gM.Story.pinguino = true;

            gM.ActiveParty.Pinguino = pingu;
        }
    }
}
