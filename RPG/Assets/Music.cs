using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public AudioClip music;
    void Start()
    {
        SoundManager.Instance.PlayMusic(music);
    }

}
