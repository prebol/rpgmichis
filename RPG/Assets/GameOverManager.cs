using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public AudioClip music;
    private void Start()
    {
        SoundManager.Instance.PlayMusic(music);
    }

    public void ReturnMenu()
    {
        ChangeSceneManager.Instance.cargarScene("MainMenu");
    }

}
