using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class INT_Pingu : Interaccion
{
    // Start is called before the first frame update
    public PartyMember pingu;
    GameManager gM;
    public string textToDisplay;
    public int tama�oTxt;
    public bool progresivo;
    public float velocidadTxt;
    private CanvasRenderer msgCanvas;
    private TextMeshProUGUI msgTxt;
    private GameObject michi;

    private void Start()
    {
        michi = GameObject.Find("Michi");
        msgCanvas = transform.Find("Canvas").transform.Find("msg").GetComponent<CanvasRenderer>();
        msgTxt = msgCanvas.transform.Find("msgTxt").GetComponent<TextMeshProUGUI>();

        msgTxt.fontSize = tama�oTxt;

        gM = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (gM.Story.pinguino)
        {
            Destroy(this.gameObject);
        }  
    }

    public override bool interactuar()
    {
       
        if (!gM.Story.quest0)
        {
            msgTxt.fontSize = 30;
            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Dejame cacho mierda";
            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else
            {
                mostrarCartelInsta();
            }  

            return true;
        }
        else if (gM.Story.perroJoined && !gM.Story.nutria)
        {
            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Hombre, dogo que tal?, bueno me da igual ale tirad";

            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }
            else
            {
                mostrarCartelInsta();
            }

            return true;

        }
        else if (gM.Story.nutria && !gM.Story.pinguino)
        {
            cartelOpen = true;
            michi.GetComponent<Movement>().canMove = false;
            textToDisplay = "Nutria estas con ellos, donde vais, espero que no al agua que no se nadar? esperad que me uno a vuestra batalla";

            if (progresivo)
            {
                StartCoroutine(mostrarCartelProgresivo());
            }  
            else
            {
                mostrarCartelInsta();
            }

            return true;

        }  
        return true;

    }

    public bool cartelOpen = false;

      

    void Update()
    {  
        if (cartelOpen)
        {
            if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape))
            {
                ocultarCartel();
            }
        }

    }



    IEnumerator mostrarCartelProgresivo()
    {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = "";
        foreach (char c in textToDisplay.ToCharArray())
        {
            msgTxt.text += c;
            yield return new WaitForSeconds(velocidadTxt);
        }
    }

    public void mostrarCartelInsta()
    {
        msgCanvas.gameObject.SetActive(true);
        msgTxt.text = textToDisplay;
    }

    public void ocultarCartel()
    {
        StopAllCoroutines();
        michi.GetComponent<Movement>().canMove = true;
        msgCanvas.gameObject.SetActive(false);
        cartelOpen = false;
          
        
        
        if (gM.Story.nutria && !gM.Story.pinguino)
        {

            gM.Story.pinguino = true;
            
            gM.ActiveParty.Pinguino = pingu;
            Destroy(this.gameObject);

        }  

    }
}
