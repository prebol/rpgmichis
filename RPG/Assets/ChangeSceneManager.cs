using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneManager : MonoBehaviour
{
    public AudioClip[] music;
    public static ChangeSceneManager Instance;
    private CanvasRenderer blackScreen;
    private Animator anim;
    
    void Awake()
    {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        
    }

    public void activarTransicion() {
        blackScreen = GameObject.Find("blackScreen").GetComponent<CanvasRenderer>();
        anim = blackScreen.GetComponent<Animator>();
        anim.Play("cerrar");
    }

    public void cargarScene(string sceneName) {
        switch (sceneName)
        {
            case "MainMenu":
                SoundManager.Instance.PlayMusic(music[0]);
                break;
            case "Mapa_Nieve":
                SoundManager.Instance.PlayMusic(music[1]);
                break;
            case "Tienda_Inside":
                SoundManager.Instance.PlayMusic(music[2]);
                break;
            case "CombateMap_1":
                SoundManager.Instance.PlayMusic(music[3]);
                break;
            case "CombateMap_Boss":
                SoundManager.Instance.PlayMusic(music[4]);
                break;
            case "GameOver":
                SoundManager.Instance.PlayMusic(music[5]);
                break;
        }
        SceneManager.LoadScene(sceneName);
    }
}
